FROM waleedka/modern-deep-learning

# File Author / Maintainer
MAINTAINER Prajwal Shreyas

# Copy the application folder inside the container
ADD /app_face_rec_train /app_face_rec_train

# install jupyter Kernel gateway
RUN pip install jupyter_kernel_gateway
RUN pip install pymysql
#RUN pip install dlib

# Get pip to download and install requirements:
RUN pip install -r /app_face_rec_train/requirements.txt

# Expose ports
#EXPOSE 8000
EXPOSE 99

# Set the default directory where CMD will execute
WORKDIR /app_face_rec_train
CMD python3 face_rec_train.py
