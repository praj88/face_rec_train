import os
import base64  #Convert img file to a base64 encoded text
import requests
import json
import urllib.parse

def face_rec_train_api(email_json):
    head= {"Content-type": "application/json"}
    response = requests.post('https://www.seelabs.app/face_rec_train', json = email_json, headers = head)
    result  = response.content.decode('utf8')
    # Replace single quotes with double quotes
    if result is not None:
        print('Face Detection parsed:{}'.format(result))
        #result = json.loads(result.replace("'", '"')) #

    return result


email_json = [{"email": "prajwal88@gmail.com"}]
result = face_rec_train_api(email_json)
