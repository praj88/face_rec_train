"""An example of how to use your own dataset to train a classifier that recognizes people.
"""
# MIT License
#
# Copyright (c) 2016 David Sandberg
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf
import numpy as np
import argparse
import os
import sys
import math
import pickle
from sklearn.svm import SVC
import os
import shutil
import boto3
from boto3.s3.transfer import S3Transfer
# os.chdir('/Users/prajwalshreyas/Desktop/Singularity/dockerapps/face_rec_train/app_face_rec_train')
from config import *
from facenet_master.src import facenet
import logging

logging.basicConfig(stream=sys.stderr, level=logging.WARNING)

def main(mode,data_dir, model, classifier_filename, use_split_dataset, test_data_dir, batch_size, image_size, seed, min_nrof_images_per_class, nrof_train_images_per_class):

    with tf.Graph().as_default():

        with tf.Session() as sess:

            np.random.seed(seed=seed)

            if use_split_dataset:
                dataset_tmp = facenet.get_dataset(data_dir)
                train_set, test_set = split_dataset(dataset_tmp, min_nrof_images_per_class, nrof_train_images_per_class)
                if (mode=='TRAIN'):
                    dataset = train_set
                elif (mode=='CLASSIFY'):
                    dataset = test_set
            else:
                dataset = facenet.get_dataset(data_dir)

            # Check that there are at least one training image per class
            for cls in dataset:
                print(cls.image_paths)
                assert(len(cls.image_paths)>0, 'There must be at least one image for each class in the dataset')


            paths, labels = facenet.get_image_paths_and_labels(dataset)

            print('Number of classes: %d' % len(dataset))
            print('Number of images: %d' % len(paths))

            # Load the model
            print('Loading feature extraction model')
            facenet.load_model(model)

            # Get input and output tensors
            images_placeholder = tf.get_default_graph().get_tensor_by_name("input:0")
            embeddings = tf.get_default_graph().get_tensor_by_name("embeddings:0")
            phase_train_placeholder = tf.get_default_graph().get_tensor_by_name("phase_train:0")
            embedding_size = embeddings.get_shape()[1]

            # Run forward pass to calculate embeddings
            print('Calculating features for images')
            nrof_images = len(paths)
            nrof_batches_per_epoch = int(math.ceil(1.0*nrof_images / batch_size))
            emb_array = np.zeros((nrof_images, embedding_size))
            for i in range(nrof_batches_per_epoch):
                start_index = i*batch_size
                end_index = min((i+1)*batch_size, nrof_images)
                paths_batch = paths[start_index:end_index]
                images = facenet.load_data(paths_batch, False, False, image_size)
                feed_dict = { images_placeholder:images, phase_train_placeholder:False }
                emb_array[start_index:end_index,:] = sess.run(embeddings, feed_dict=feed_dict)

            classifier_filename_exp = os.path.expanduser(classifier_filename)
            print(classifier_filename_exp)
            if (mode=='TRAIN'):
                # Train classifier
                print('Training classifier')
                model = SVC(kernel='linear', probability=True)
                model.fit(emb_array, labels)

                # Create a list of class names
                class_names = [ cls.name.replace('_', ' ') for cls in dataset]

                # Saving classifier model
                with open(classifier_filename_exp, 'wb') as outfile:
                    pickle.dump((model, class_names), outfile)
                print('Saved classifier model to file "%s"' % classifier_filename_exp)

            elif (mode=='CLASSIFY'):
                # Classify images
                print('Testing classifier')
                with open(classifier_filename_exp, 'rb') as infile:
                    (model, class_names) = pickle.load(infile)

                print('Loaded classifier model from file "%s"' % classifier_filename_exp)

                predictions = model.predict_proba(emb_array)
                best_class_indices = np.argmax(predictions, axis=1)
                best_class_probabilities = predictions[np.arange(len(best_class_indices)), best_class_indices]

                for i in range(len(best_class_indices)):
                    print('%4d  %s: %.3f' % (i, class_names[best_class_indices[i]], best_class_probabilities[i]))

                accuracy = np.mean(np.equal(best_class_indices, labels))
                print('Accuracy: %.3f' % accuracy)


def split_dataset(dataset, min_nrof_images_per_class, nrof_train_images_per_class):
    train_set = []
    test_set = []
    for cls in dataset:
        paths = cls.image_paths
        # Remove classes with less than min_nrof_images_per_class
        if len(paths)>=min_nrof_images_per_class:
            np.random.shuffle(paths)
            train_set.append(facenet.ImageClass(cls.name, paths[:nrof_train_images_per_class]))
            test_set.append(facenet.ImageClass(cls.name, paths[nrof_train_images_per_class:]))
    return train_set, test_set

def upload_s3(dirpath, bucket, filepath_s3):
    print('Start of Upload')
    filename = os.path.basename(dirpath)
    s3 = boto3.client('s3', region_name='us-east-1',aws_access_key_id = AWS_prajwal_credetials['aws_access_key_id'], aws_secret_access_key = AWS_prajwal_credetials['aws_secret_access_key'])
    s3.upload_file(Bucket='face.rec.training', Key=filepath_s3, Filename=dirpath)
    logging.warning('Model uploaded to S3')
    return 'Training Complete.'

def run_classifier(email):
    bucket = 'face.rec.training'
    path = ''#'/Users/prajwalshreyas/Desktop/Singularity/dockerApps/face_rec_train'
    account_email =email# update as a variable from user account email
    account_email_fmt = account_email.split('@')[0]
    mode= 'TRAIN' # choices=['TRAIN', 'CLASSIFY'],'Indicates if a new classifier should be trained or a classification ' + 'model should be used for classification'
    inputImages = path + '/app_face_rec_train/facenet_master/src/align/inputImages/'+account_email
    outputImages = path + '/app_face_rec_train/facenet_master/src/align/output_images/'
    data_dir = outputImages+ account_email+'/processedImages/'#Path to the data directory containing aligned LFW face patches.
    model = path + '/app_face_rec_train/facenet_master/src/models/20170512-110547/20170512-110547.pb'#Could be either a directory containing the meta_file and ckpt_file or a model protobuf (.pb) file'
    classifier_filename= path + '/app_face_rec_train/my_classifier/my_classifier.pkl'#'Classifier model file name as a pickle (.pkl) file. ' +  'For training this is the output and for classification this is an input.'
    use_split_dataset = True #Indicates that the dataset specified by data_dir should be split into a training and test set. 'Otherwise a separate test set can be specified using the test_data_dir option.'
    test_data_dir = path + '/app_face_rec_train/facenet_master/test_images/'#''Path to the test data directory containing aligned images used for testing.'
    batch_size=2 #Number of images to process in a batch.'
    image_size=160# 'Image size (height, width) in pixels.'
    seed = 666 #Random seed
    min_nrof_images_per_class = 5 #'Only include classes with at least this number of images in the dataset'
    nrof_train_images_per_class =15 #'Use this number of images from each class for training and the rest for testing'
    classifier_s3_path = account_email+'/my_classifier.pkl'

    main(mode, data_dir, model, classifier_filename, use_split_dataset, test_data_dir, batch_size, image_size, seed, min_nrof_images_per_class, nrof_train_images_per_class)

    #upload model classifer to s3
    print(classifier_s3_path)
    result = upload_s3(classifier_filename, bucket, classifier_s3_path)

    #Delete training images
    # output images
    shutil.rmtree(outputImages+ account_email)
    # input images
    shutil.rmtree(inputImages)

    return result
