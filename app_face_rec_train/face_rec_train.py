#Import the OpenCV and dlib libraries
import logging
import numpy as np
import time
import pandas as pd
import glob
import os
from flask import Flask, render_template, request, redirect, url_for, send_from_directory
from werkzeug import secure_filename
import boto3 # AWS Packages
from boto3.s3.transfer import S3Transfer # AWS Packages
#os.chdir('/Users/prajwalshreyas/Desktop/Singularity/dockerApps/face_rec_train/app_face_rec_train')
from config import *
import pymysql
from io import StringIO, BytesIO
import json
from facenet_master.src.align import align_dataset_mtcnn
from facenet_master.src import classifier
import datetime as dt

#Flask api initialise
app = Flask(__name__)

#rds settings
rds_host  = "sqlcam.c3cxjwruoslb.us-east-1.rds.amazonaws.com"
rds_name = db_username
rds_password = db_password
db_name1 = db_name1
db_name2 = db_name2

def connect_db(db_name):
    conn = pymysql.connect(rds_host, user=rds_name, passwd=rds_password, db=db_name, connect_timeout=30)
    return conn

def insert_logs(timestamp, train_flag, email):
    # Write new log into database --------
    conn = connect_db(db_name2)
    try:
        with conn.cursor() as cur:
            cur.execute("INSERT INTO face_rec_train_logs (timestamp, train_flag, email) VALUES (%s, %s, %s)",(timestamp, train_flag, email))
            conn.commit()
            conn.close()
            status = 'Successfully inserted logs into DB'
    except Exception as e:
            status = e
            conn.close()
    return status



# API set up  ---------------------------------------------------------------------------------------------
@app.route('/face_rec_train_test')
def face_rec_train_test():
    return 'Face Rec Training API working'


@app.route('/face_rec_train', methods=['POST'])
def face_rec_train():

    if request.method == 'POST' and request.is_json:

        req_json = pd.DataFrame(request.json)
        logging.warning('dataframe read')
        email = req_json['email'][0] #account email id
        print('Email:{}'.format(email))

        logging.warning('Start of Face Rec Training')
        result = 'Error in processing, please ensure the format of the file is as per the documentation.'
        # Get the name of the uploaded file
        logging.warning(request)


        try:
            # Run function to align images
            align_dataset_mtcnn.align_images(email)
            # Retrain the classifier
            result = classifier.run_classifier(email)
            time_now = dt.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
            insert_logs(time_now,'True', email)
        except Exception as e:
               logging.warning(e)
               result = str(e) #'Error while processing the image.'

        return result




if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=True, port=99)
